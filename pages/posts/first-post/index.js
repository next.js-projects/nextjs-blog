import Head from 'next/head'
import Link from 'next/link'

import Style from './style'

export default function FirstPost() {

  return (
    <>
      <Head>
        <title>First Post</title>
      </Head>
      <h1>First Post</h1>
      <img src="/vercel.svg" alt="Vercel Logo" className="logo" />
      <h2>
        <Link href="/"><a className="test">Back to Home</a></Link>
      </h2>

      <Style />
    </>
  )

}